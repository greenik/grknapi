package src;

import static org.apache.commons.io.FileUtils.copyURLToFile;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import src.Window;

public class Opensubtitles {
	//URL SERVERA API
    private static String OSDB_SERVER = "http://api.opensubtitles.org/xml-rpc";
    //TOKEN Z SERWERA API
    private static String strToken = null;
    
    public static void logIn() throws Exception {
        try {
        XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
        rpcConfig.setServerURL(new URL(OSDB_SERVER));
        XmlRpcClient rpcClient = new XmlRpcClient();
        rpcClient.setConfig(rpcConfig);
        Object[] objParams = new Object[]{"greeniq", "zielony5", "pol", "grkNapi"};
        HashMap<?, ?> mapResult = (HashMap<?, ?>) rpcClient.execute("LogIn", objParams);
        strToken = (String) mapResult.get("token");
        }
        catch(Exception e) {
            System.out.println("[ERROR] " + e.getMessage());
        }
    }
    
    public static Boolean searchForSubtitles(String hash, File file, String language) throws Exception {
        try {
            logIn();
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.getMessage());
        }
        try {
            
            FileInputStream fisInputStream = new FileInputStream(file);
            FileChannel fchFileChannel = fisInputStream.getChannel();
            
            XmlRpcClientConfigImpl rpcConfig = new XmlRpcClientConfigImpl();
            rpcConfig.setServerURL(new URL(OSDB_SERVER));
            XmlRpcClient rpcClient = new XmlRpcClient();
            rpcConfig.setEnabledForExtensions(true);
            rpcClient.setConfig(rpcConfig);
            Map<String, Object> mapQuery = new HashMap<String, Object>();
            //mapQuery.put("sublanguageid", Locale.getDefault().getISO3Language());
            mapQuery.put("sublanguageid", language);
            mapQuery.put("moviehash", Hasher.computeHash(file));
            mapQuery.put("moviebytesize", fchFileChannel.size());
            Object[] objParams = new Object[]{strToken, new Object[]{mapQuery}};
            HashMap<?, ?> x = (HashMap<?, ?>) rpcClient.execute("SearchSubtitles", objParams);
            Object[] lstData = (Object[]) x.get("data");
            System.out.println(lstData);
            HashMap<?, ?> mapResult = (HashMap<?, ?>) lstData[0];
            URL urlSubtitle = new URL((String) mapResult.get("SubDownloadLink"));
            String x1 = FilenameUtils.getBaseName(file.getAbsolutePath());
            String filePath = file.getAbsolutePath().substring(0,file.getAbsolutePath().lastIndexOf(File.separator));
            //System.out.println(filePath);
            copyURLToFile(urlSubtitle, new File(filePath + "/" + x1 + ".gz"));
            Unzip.gunzipIt(filePath + "/" + x1 + ".gz", filePath + "/" + x1 + ".txt");
            File subArchFile = new File(filePath + "/" + x1 + ".gz");
            subArchFile.delete();
            System.out.println("Rozpakowano");
            return true;
        } catch (Exception e) {
            System.out.println("[ERROR] " + e.getMessage());
            return false;
        }
    }
}
